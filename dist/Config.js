var TapConfig = {
    tourMLEndpoint: '/tourml.xml',
    trackerID: '',
    media: {
        pluginPath: 'dist/vendor/mediaelement/'
    },
    geo: {},
    social: {},
    tourSettings: {
        'default': {
            'defaultNavigationController': 'StopListView',
            'enabledNavigationControllers': ['StopListView']
        }
    },
    navigationControllers: {
    'StopListView': {
        label: 'Stop Menu',
        filterBy: 'code',
        sortBy: 'code',
        displayCodes: false
    	}
    },
    viewRegistry: {},
    primaryRouter: "Default"
};
